# HTB

En este proyecto pretendo reunir los writeups de las máquinas de la plataforma de CTF "Hack the box" que voy resolviendo.

Ya hay muchos writeups de calidad en internet de otros jugadores con una experiencia y un ingenio asombroso pero es una buena práctica, ampliamente recomendada, escribir el writeup a medida que se va resolviendo una máquina para repasar lo aprendido por el camino.

[ ![Relwarc17](https://www.hackthebox.eu/badge/image/191443)](https://www.hackthebox.eu/profile/191443)